import boto3


def lambda_handler(event, context):
    ddbClient = boto3.resource('dynamodb')
    ddbTable = ddbClient.Table('Users')
    response = ddbTable.put_item(
        Item={
            'id': event['id'],
            'name': event['name'],
            'address': event['address'],
            'mobile': event['mobile'],
            'dob': event['dob']
        }
    )
    return {
        'statusCode': response['ResponseMetadata']['HTTPStatusCode'],
        'body': 'Record ' + event['id'] + ' Added'
    }
