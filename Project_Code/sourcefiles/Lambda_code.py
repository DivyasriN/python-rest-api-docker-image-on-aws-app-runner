import boto3


def lambda_handler(event, context):
    client = boto3.resource('dynamodb')

    table = client.Table('Pets')

    response = table.put_item(

        Item={
            'id': event['id'],
            'name': event['name'],
            'address': event['breed'],
            'mobile': event['gender'],
            'dob': event['owner']
        }

    )

    return {

        'statusCode': response['ResponseMetadata']['HTTPStatusCode'],
        'body': 'Record ' + event['id'] + ' added'
    }
